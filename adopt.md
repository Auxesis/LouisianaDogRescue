---
permalink: /adopt
title: Adoptable Dogs
description: 
---

{% include header.html %}

<body class="profile-page">
	{% include navigation.html %}

    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('assets/img/temp_bg.jpg');"></div>

		<!--- Start Content --->
		
		<div class="main main-raised">
			<div class="container">
				<div class="section text-center section-landing" id="about">
				
				{% for dog in site.adoptable %}
				<a href="{{ site.url }}{{ dog.url }}" class="btn btn-primary" >
				
				<img src="http://placehold.it/200x200"><br/>
				<b>{{ dog.name }}</b><br/>
				<i>{{ dog.breed }}</i><br/>
				{{ dog.description }}<br/>
				<i>{{ dog.rescue_date | date_to_long_string }}</i>
				
				</a>
				{% endfor %}
				
				</div>
			</div>
		</div>
		
		<!-- End Content -->
		
		{% include footer.html %}
	</div>
</body>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>