---
title: Louisiana Dog Rescue
layout: null
---
{% include header.html %}

<body class="landing-page">
	{% include navigation.html %}

	<div class="wrapper">
		<div class="header header-filter" style="background-image: url('assets/img/temp_bg.jpg');">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1 class="title">{{ site.summary }}</h1>
						<h4>{{ site.description }}</h4>
						<br />
						<a href="{{ site.video }}" class="btn btn-danger btn-raised btn-lg">
							<i class="fa fa-play"></i> Watch video
						</a>
					</div>
				</div>
			</div>
		</div>
			
		<!-- Start Content -->

		<div class="main main-raised">
			<div class="container">
				<div class="section text-center section-landing" id="about">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h2 class="title">Let's talk product</h2>
							<h5 class="description">This is the paragraph where you can write more details about your product. Keep you user engaged by providing meaningful information. Remember that by this time, the user is curious, otherwise he wouldn't scroll to get here. Add a button if you want the user to see more.</h5>
						</div>
					</div>

					<div class="features">
						<div class="row">
							<div class="col-md-4">
								<div class="info">
									<div class="icon icon-primary">
										<i class="material-icons">chat</i>
									</div>
									<h4 class="info-title">First Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="info">
									<div class="icon icon-success">
										<i class="material-icons">verified_user</i>
									</div>
									<h4 class="info-title">Second Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="info">
									<div class="icon icon-danger">
										<i class="material-icons">fingerprint</i>
									</div>
									<h4 class="info-title">Third Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="section text-center section-landing" id="get-involved">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h2 class="title">Let's talk product</h2>
							<h5 class="description">This is the paragraph where you can write more details about your product. Keep you user engaged by providing meaningful information. Remember that by this time, the user is curious, otherwise he wouldn't scroll to get here. Add a button if you want the user to see more.</h5>
						</div>
					</div>

					<div class="features">
						<div class="row">
							<div class="col-md-4">
								<div class="info">
									<div class="icon icon-primary">
										<i class="material-icons">chat</i>
									</div>
									<h4 class="info-title">First Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="info">
									<div class="icon icon-success">
										<i class="material-icons">verified_user</i>
									</div>
									<h4 class="info-title">Second Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="info">
									<div class="icon icon-danger">
										<i class="material-icons">fingerprint</i>
									</div>
									<h4 class="info-title">Third Feature</h4>
									<p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<!-- End Content -->
		
		{% include footer.html %}
	</div>
</body>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="assets/js/material-kit.js" type="text/javascript"></script>

</html>
