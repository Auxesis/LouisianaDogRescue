---
layout: profile
image:
thumbnail: /assets/img/temp_bg.jpg
images:
 - /images/dogs/default_image1.png
 - /images/dogs/default_image2.png
name: Spot
sex: male
age: 1yr
breed: Mutt
weight: 35ilbs
adoption_fee: $50
housebroken: true
kids: true
cats: true
dogs: true
rescue_date: April 13, 2017
adopted_date: April 13, 2017
description: This is a short summary that appears in search engine results
---

Spot is a one year old mutt that weighs 35 pounds. He was rescued on April 13, 2017, and was adopted April 13, 2017.